package command;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import exception.UniqueCommandException;

public class Commands  {
	
	private ArrayList<Command> commands;
	private CmdListType cmdListType;
	
	public Commands(CmdListType cmdListTypee) {
		this.cmdListType = cmdListType;
	}
	
	public void addCommand(Command command) throws UniqueCommandException {
		if (cmdListType.equals(CmdListType.UNIQUE) && commands.stream().filter( c -> c.getName().equalsIgnoreCase(command.getName()) ).count() > 0) {
			throw new UniqueCommandException();
		}
		commands.add(command);
	}
	
	public int run(Consumer<Command> cmd) {
		List<Command> cmds = commands.stream().peek( cmd ).collect(Collectors.toList());
		return cmds.size();
	}
	
	public Command findByName(String name) {
		Optional<Command> command = commands.stream().filter( c -> c.getName().equalsIgnoreCase(name) ).findFirst();		
		return command.isPresent()?command.get():null;
	}
	
	public enum CmdListType {
		UNIQUE, ANY;
	}
	
	

}
