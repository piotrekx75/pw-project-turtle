package command;

import java.util.stream.Collectors;

import exception.UniqueCommandException;
import gui.Canvas;
import turtle.Turtle;

public class ComplexCommand extends Command {
	
	protected Commands commands = new Commands(Commands.CmdListType.ANY);

	public ComplexCommand(String name) {
		super(name);
	}
	
	public void addCommand(Command command)  {
		try {
		commands.addCommand(command);
		} catch(UniqueCommandException e) {			
		}
	}

	@Override
	public void run(Turtle turtle, Canvas canvas, Canvas showCanvas) {
	   commands.run( cmd -> cmd.run(turtle, canvas, showCanvas) );	   
	}
	
	public Command make(String[] params) { return null; }

}
