package command;

import exception.UniqueCommandException;
import gui.Canvas;
import turtle.Turtle;

public abstract class Command {
	
	private String name;
	
	public Command(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	abstract public void run(Turtle turtle, Canvas canvas, Canvas showCanvas);
	
	public boolean isCmd(String line) {
		return line.toLowerCase().startsWith(name.toLowerCase());
	}
	
	public String[] readParams(String line) {
		String[] params = new String[5];
		return params;
	}
	
	abstract public Command make(String[] params);
	
	public boolean process(String line, Commands commands) throws UniqueCommandException {
		if (isCmd(line)) {
			commands.addCommand(make(readParams(line)));
			return true;
		} else { return false; }
	}
	

}
