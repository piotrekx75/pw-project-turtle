package command.defined;

import command.Command;
import command.ComplexCommand;
import gui.Canvas;
import turtle.Position;
import turtle.Turtle;

public class CommandMove extends Command {
	
	public static final String NAME_COMMAND_MOVE = "MOVE";
	private Double distance = 0.0;	
	
	public CommandMove() {
		super(NAME_COMMAND_MOVE);
	}	
	
	public CommandMove(Double distance) {
		this();
		this.distance = distance;
	}

	@Override
	public void run(Turtle turtle, Canvas canvas, Canvas showCanvas) {
		Position newPosition = getNewPos(turtle);
		turtle.drawLine(canvas, newPosition);
//	    showCanvas.copy(canvas);
		turtle.drawTurtle(showCanvas);
	}
	
	public Position getNewPos(Turtle turtle) {
		Double fromX = turtle.getPosX();
		Double fromY = turtle.getPosY();
		Double toX = fromX;
		Double toY = fromY;
		return new Position(toX, toY);
	}
	
	@Override
	public Command make(String[] params) { 
		  CommandMove cmd = new CommandMove(Double.valueOf(params[0]));
		  return cmd; 
	}

}


