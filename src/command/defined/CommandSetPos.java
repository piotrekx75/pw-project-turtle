package command.defined;

import command.Command;
import command.SimpleCommand;
import gui.Canvas;
import turtle.Turtle;

public class CommandSetPos extends SimpleCommand {

	public static final String NAME_COMMAND_SET = "SET";
	private Double x = 0.0;
	private Double y = 0.0;
	private Double angle = 0.0;

	public CommandSetPos() {
		super(NAME_COMMAND_SET);
	}

	public CommandSetPos(Double x, Double y, Double angle) {
		this();
		this.x = x;
		this.y = y;
		this.angle = angle;
	}

	@Override
	public void doCmd(Turtle turtle, Canvas canvas) {
		turtle.setPosXY(this.x, this.y);
		turtle.setAngle(angle);
	}

	@Override
	public Command make(String[] params) {
		CommandSetPos cmd = new CommandSetPos(Double.valueOf(params[0]), Double.valueOf(params[1]),
				Double.valueOf(params[2]));
		return cmd;
	}

}
