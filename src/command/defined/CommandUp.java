package command.defined;

import command.Command;
import command.SimpleCommand;
import gui.Canvas;
import turtle.Turtle;

public final class CommandUp extends SimpleCommand {
	
	private static final String NAME_COMMAND_UP = "Up";
	
	public CommandUp() {
		super(NAME_COMMAND_UP);
	}

	@Override
	public void doCmd(Turtle turtle, Canvas canvas) {
		turtle.up();
	}
	
	

}
