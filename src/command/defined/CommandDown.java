package command.defined;

import command.Command;
import command.SimpleCommand;
import gui.Canvas;
import turtle.Turtle;

public final class CommandDown extends SimpleCommand {

	private static final String NAME_COMMAND_DOWN = "DOWN";
	
	public CommandDown() {
		super(NAME_COMMAND_DOWN);
	}

	@Override
	public void doCmd(Turtle turtle, Canvas canvas) {
		turtle.down();
	}
	
}
