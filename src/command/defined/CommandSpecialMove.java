package command.defined;

import command.ComplexCommand;

public class CommandSpecialMove extends ComplexCommand {
	
	public static final String NAME_COMMAND_SPEC_MOVE = "SPEC_MOVE";
	
	private int level;
	private Double distance;	

	public CommandSpecialMove() {
		super(NAME_COMMAND_SPEC_MOVE);		
	}
	
	public CommandSpecialMove(Double distance, int level) {
		this();
		this.distance = distance;
		this.level = level;		
		makeCommands();
	}
	
	public void makeCommands() {		
		if (level <= 1) {
			addCommand(new CommandMove(distance));			
		}
		else {
			Double pr1_4 = distance / 0.25;
			Double pr = Math.sqrt( 2 * Math.pow(pr1_4, 2) ); 
			addCommand(new CommandSpecialMove(pr1_4, level-1));
			addCommand(new CommandRotate(45.0, CommandRotate.Direction.ANTICLOCKWISE));
			addCommand(new CommandSpecialMove(pr, level-1));
			addCommand(new CommandRotate(90.0, CommandRotate.Direction.CLOCKWISE));
			addCommand(new CommandSpecialMove(pr, level-1));
			addCommand(new CommandRotate(45.0, CommandRotate.Direction.ANTICLOCKWISE));
			addCommand(new CommandSpecialMove(pr1_4 / 0.25, level-1));
		}
		
	}
	
}
