package command.defined;

import command.Command;
import command.SimpleCommand;
import gui.Canvas;
import turtle.Turtle;

public class CommandRotate extends SimpleCommand {

	public static final String NAME_COMMAND_ROTATE = "SET";
	private Double diffAngle = 0.0;
	private Direction direction = Direction.CLOCKWISE;
	
	public CommandRotate() {
		super(NAME_COMMAND_ROTATE);
	}	

	public CommandRotate(Double diffAngle, Direction direction) {
		this();		
		this.diffAngle = diffAngle;
		this.direction = direction;
	}

	@Override
	public void doCmd(Turtle turtle, Canvas canvas) {
		
	}

	public enum Direction {
		CLOCKWISE, ANTICLOCKWISE;
	}
	
}
