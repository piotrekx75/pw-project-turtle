package command.defined;

import command.Command;
import command.SimpleCommand;
import gui.Canvas;
import turtle.Turtle;

public class CommandSetColor extends SimpleCommand {
	
	private Color color;
	
	public static final String NAME_COMMAND_SET_COLOR = "SET_COLOR";

	public CommandSetColor() {
		super(NAME_COMMAND_SET_COLOR);
	}
	
	public CommandSetColor(Color color) {
		this.color = color;
	}

	@Override
	public void doCmd(Turtle turtle, Canvas canvas) {
		turtle.setColor(color);
	}
	
	@Override
	public Command make(String[] params) { 
		CommandSetColor cmd = new CommandSetColor(toColor(params[0]));
		  return cmd; 
	}
	
	public static Color toColor(String clr) {
		return 
	}


}
