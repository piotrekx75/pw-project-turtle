package command;
import gui.Canvas;
import turtle.Turtle;

public abstract class SimpleCommand extends Command {

	public SimpleCommand(String name) {
		super(name);
	}

	@Override
	public Command make(String[] params) { return null; }

	@Override
	public void run(Turtle turtle, Canvas canvas, Canvas showCanvas) {
		doCmd(turtle, canvas);
//	    showCanvas.copy(canvas);
		turtle.drawTurtle(showCanvas);
	}
	
	abstract public void doCmd(Turtle turtle, Canvas canvas);	

}
