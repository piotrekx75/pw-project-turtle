package gui;

import command.Commands;
import turtle.Turtle;

public class CanvasCommandRunner {
	
	private Canvas canvas;
	private Canvas showCanvas;
	private Turtle turtle;	
	
	public CanvasCommandRunner(Canvas canvas, Canvas showCanvas, Turtle turtle) {
		this.canvas = canvas;
	}
	
	public int run(Commands commands) {
		return commands.run(  cmd ->  cmd.run(turtle, canvas, showCanvas) );
	}

}
