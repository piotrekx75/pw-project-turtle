package turtle;
import java.awt.Color;

import gui.Canvas;

public class Turtle {
	
	private Position pos;
	private Double angle;
	private Color color; 
	private State state;
	
//	private Image turtleImage; 
	
	public Turtle( ) {
		state = State.UP;
		pos = new Position(0.0,0.0);
		color = java.awt.Color.BLACK;
	}
	
//	public loadTurtleImage(Image img) {
//		
//	}
	
	public void drawLine(Canvas canvas, Position newPos) {		
//		if (isDown()) {
//			newPos = canvas.drawLine(pos, newPos, color);
//		}
//		else {
//		  newPos = canvas.symDrawLine( pos, newPos );
//		 }
//		pos.copy(newPos);
	}
	
	public void drawTurtle(Canvas canvas) {
//		canvas.drawImage(img);
	}
	
	public boolean isDown() {
		return state == State.DOWN;
	}
	
	public void up() {
		state = State.UP;
	}	
	
	public void down() {
		state = State.DOWN;
	}
	
	public void setPosXY(Double x, Double y) {
		pos.setXY(x, y);
	}
	
	public Double getPosX() {
		return pos.getX();
	}
	
	public Double getPosY() {
		return pos.getY();
	}
	
	public void setAngle(Double angle) {
		this.angle = angle;
	}
	
	public enum State {
		UP, DOWN;
	}

}
