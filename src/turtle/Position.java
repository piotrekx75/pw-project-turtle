package turtle;

public class Position {
	
	private Double x;
	private Double y;
	
	public Position(Double x, Double y) {
		this.x = x;
		this.y = y;
	}
	
	public void setXY(Double x, Double y) {
		this.x = x;
		this.y = y;
	}
	
	public Double getX() {
		return this.x;
	}
	
	public Double getY() {
		return this.y;
	}

}
